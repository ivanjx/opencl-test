﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OpenCL.NetCore;
using OpenCL.NetCore.Extensions;

namespace opencl_test
{
    class opencl_test
    {
        // OpenCL wrapper: https://github.com/realivanjx/OpenCL.NetCore
        // Source: https://www.youtube.com/watch?v=f8jnAuFMnPY&list=PLzy5q1NUJKCJocUKsRxZ0IPz29p38xeM-&index=6
        static void Main(string[] args)
        {
            // Getting platform.
            ErrorCode err;
            Platform[] platforms = Cl.GetPlatformIDs(out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to get platform: {0}", err.ToString());
                return;
            }

            Platform platform = platforms.First();

            // Getting devices.
            Device[] devices = Cl.GetDeviceIDs(platform, DeviceType.All, out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to get device: {0}", err.ToString());
                return;
            }

            if (devices.Length == 0)
            {
                Console.WriteLine("No devices found.");
                return;
            }

            // Getting device info.
            Device device = devices.First();
            string name = Cl.GetDeviceInfo(device, DeviceInfo.Name, out err).ToString();
            string version = Cl.GetDeviceInfo(device, DeviceInfo.Version, out err).ToString();
            string vendor = Cl.GetDeviceInfo(device, DeviceInfo.Vendor, out err).ToString();
            Console.WriteLine("Device: {0} ({1}) - {2}", name, version, vendor);

            // Creating context.
            Context context = Cl.CreateContext(
                null,
                (uint)1,
                new[] { device },
                null,
                IntPtr.Zero,
                out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to create context: {0}", err.ToString());
                return;
            }

            // Reading kernel.
            string kernelSrc = File.ReadAllText("kernel.cl");

            // Creating program.
            Program program = Cl.CreateProgramWithSource(
                context,
                1,
                new[] { kernelSrc },
                new[] { (IntPtr)kernelSrc.Length },
                out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to create program: {0}", err.ToString());
                return;
            }

            // Building program.
            err = Cl.BuildProgram(
                program,
                (uint)1,
                new[] { device },
                "-cl-std=CL1.2",
                null,
                IntPtr.Zero);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to build program: {0}", err.ToString());
                return;
            }

            // Creating buffer.
            byte[] buff = new byte[12];
            IMem membuff = Cl.CreateBuffer(
                context,
                MemFlags.WriteOnly,
                (IntPtr)buff.Length,
                IntPtr.Zero,
                out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to create buffer: {0}", err.ToString());
                return;
            }

            // Creating kernel.
            Kernel kernel = Cl.CreateKernel(
                program,
                "hello_world",
                out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to create kernel: {0}", err.ToString());
                return;
            }

            // Creating command queue.
            CommandQueue queue = Cl.CreateCommandQueue(
                context,
                device,
                CommandQueueProperties.None,
                out err);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to create queue: {0}", err.ToString());
                return;
            }

            // Setup kernel parameter.
            err = Cl.SetKernelArg(
                kernel,
                0,
                (IntPtr)Marshal.SizeOf(typeof(IntPtr)),
                membuff);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to set kernel arg: {0}", err.ToString());
                return;
            }

            // Executing kernel.
            err = Cl.EnqueueTask(
                queue,
                kernel,
                0,
                null,
                out Event _);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to execute task: {0}", err.ToString());
                return;
            }

            // Get result.
            err = Cl.EnqueueReadBuffer(
                queue,
                membuff,
                Bool.True,
                0,
                buff.Length,
                buff,
                0,
                null,
                out Event _);

            if (err != ErrorCode.Success)
            {
                Console.WriteLine("Unable to read result: {0}", err.ToString());
                return;
            }

            Console.WriteLine("Result: {0}", Encoding.ASCII.GetString(buff));

            // Done.
            Cl.Finish(queue);
            Cl.ReleaseMemObject(membuff);
        }
    }
}
